<?php
/*
Template Name: Animals Archive
*/

get_header();
?>

<div id="primary" class="content-area">
  <div id="content" class="site-content" role="main">
    <?php
    $posts = new WP_Query(array(
      'post_type' => 'animals',
      'orderby' => 'meta_value',
      'meta_key' => 'animal_name',
      'order' => 'asc',
    ));
    while ( $posts->have_posts() ) : $posts->the_post();?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <!-- scommentare se serve mostrare l'immagine in evidenza del post
        <?php the_post_thumbnail( array( 100, 100 ) ); ?>
        -->

        <!-- scommentare se serve mostrare il titolo del post
        <h2><?php the_title(); ?></h2>
        -->

        <strong>Name: </strong>
        <?php echo esc_html( get_post_meta( get_the_ID(), 'animal_name', true ) ); ?>
        <br />

        <strong>Type: </strong>
        <?php echo esc_html( get_post_meta( get_the_ID(), 'animal_type', true ) ); ?>
        <br />

        <strong>Age in Months: </strong>
        <?php echo esc_html( get_post_meta( get_the_ID(), 'animal_months', true ) ); ?>
        <br />
          
      </article>

    <?php endwhile; ?>
  </div><!-- #content -->
</div><!-- #primary -->

<?php
wp_reset_query();

get_sidebar( 'content' );
get_sidebar();
get_footer();
