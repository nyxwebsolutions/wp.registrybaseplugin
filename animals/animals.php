<?php

/*
Plugin Name: Animals
Plugin URI: http://nyx.solutions
Description: A test plugin for animals archive
Version: 1.0
Author: Daniele Megna
Author URI: http://nyx.solutions
License: GPLv2
*/

/********** QUI SI DEFINISCE IL NUOVO TIPO DI POST (in questo caso 'animals') ***********/
add_action('init', 'register_animals_post_type');
function register_animals_post_type()
{
  register_post_type(
    'animals',
    array(
      'labels' => array(
        'name' => 'Animals',
        'singular_name' => 'Animal',
        'add_new_item' => 'Add New Animal',
        'edit_item' => 'Edit Animal',
        'new_item' => 'New Animal',
        'view_item' => 'View Animal'
      ),

      'public' => true,
      'menu_position' => 15,
      'supports' => array(''),  // QUI AGGIUNGERE 'title' SE SI VUOLE USARE IL TITOLO O 'thumbnail' SE SI VUOLE L'IMMAGINE IN EVIDENZA
      'taxonomies' => array(''),
      'has_archive' => true,
      'rewrite' => array('slug' => 'animalfarm'),
    )
  );
}
/********** FINE QUI SI DEFINISCE IL NUOVO TIPO DI POST 'animals' ***********/

/********** QUI SI CREA IL METABOX CON I CAMPI CUSTOM RELATIVI AL NUOVO TIPO DI POST ***********/
add_action('admin_init', 'add_animals_metabox');
function add_animals_metabox()
{
  add_meta_box(
    'animals_metabox',
    'Animal Details',
    'display_animal_metabox',
    'animals',
    'normal',
    'high'
  );
}
/********** FINE QUI SI CREA IL METABOX CON I CAMPI CUSTOM RELATIVI AL NUOVO TIPO DI POST ***********/

/********** QUESTO E' IL CONTENUTO DEL METABOX CON I NUOVI CAMPI CUSTOM (non serve tag form) ***********/
function display_animal_metabox($post)
{
  // qui recupero il valore attuale dei campi (in inserimento saranno vuoti)
  $animal_name    = esc_html(get_post_meta($post->ID, 'animal_name', true));
  $animal_type    = esc_html(get_post_meta($post->ID, 'animal_type', true));
  $animal_months  = intval(get_post_meta($post->ID, 'animal_months', true));
  ?>
  <table>
    <tr>
      <td style="padding-right:30px">Animal name</td>
      <td><input type="text" size="80" name="animal_name" value="<?php echo $animal_name; ?>" /></td>
    </tr>
    <tr>
      <td>Animal Type</td>
      <td>
        <select style="width: 200px" name="animal_type">
          <option value=''>Select an option</option>
          <option value='Dog' <?php echo selected('Dog', $animal_type); ?>>Dog</option>
          <option value='Cat' <?php echo selected('Cat', $animal_type); ?>>Cat</option>
          <option value='Bird' <?php echo selected('Bird', $animal_type); ?>>Bird</option>
        </select>
      </td>
    </tr>
    <tr>
      <td>Age in Months</td>
      <td><input type="number" name="animal_months" value="<?php echo $animal_months; ?>" /></td>
    </tr>
  </table>
  <?php
}
/********** FINE QUESTO E' IL CONTENUTO DEL METABOX CON I NUOVI CAMPI CUSTOM (non serve tag form) ***********/

/********** QUI VIENE FATTO IL SALVATAGGIO SUL DATABASE DEI CAMPI CUSTOM, DEL TITOLO E DELLO SLUG ***********/
add_action( 'save_post', 'save_animals_metabox_fields', 10, 2 );
function save_animals_metabox_fields($post_id, $post)
{
  if ($post->post_type !== 'animals')
    return;

  $fields = array(
    'animal_name',
    'animal_type',
    'animal_months',
  );

  foreach($fields as $field) {
    if (isset($_POST[$field]) && $_POST[$field] != '')
      update_post_meta($post_id, $field, $_POST[$field]);    
  }

  /** TOGLIERE QUESTA PARTE SE SI USA IL TITOLO **/
  $title_field = 'animal_name'; // qui si definisce quale dei campo custom usare come titolo e slug
  if ($title_field !== null && isset($_POST[$title_field]) && $_POST[$title_field] != '') {
    $new_title = $_POST[$title_field];
    $new_slug = sanitize_title($new_title);

    remove_action('save_post', 'save_animals_metabox_fields');
    wp_update_post(array('ID' => $post_id, 'post_title' => $new_title, 'post_name' => $new_slug));
    add_action('save_post', 'save_animals_metabox_fields');
  }
  /** FINE TOGLIERE QUESTA PARTE SE SI USA IL TITOLO **/

}
/********** FINE QUI VIENE FATTO IL SALVATAGGIO SUL DATABASE DEI CAMPI CUSTOM, DEL TITOLO E DELLO SLUG ***********/

/**********  QUI VIENE DEFINITO QUALE DELLE COLONNE FAR VEDERE NELL'ELENCO NELLA PAGINA ADMIN E CON QUALE LABEL DI INTESTAZIONE ***********/
add_filter('manage_animals_posts_columns', 'add_animals_admin_columns');
function add_animals_admin_columns($columns)
{
  $columns['title'] = 'Name';

  // SCOMMENTARE QUESTA RIGA SE SI USA IL TITOLO
  //$columns['animal_name'] = 'Name';
  $columns['animal_type'] = 'Type';
  $columns['animal_months'] = 'Age in Months';
  
  // spostiamo la data come ultima colonna
  $tmp = $columns['date'];
  unset($columns['date']);
  $columns['date'] = $tmp;

  return $columns;
}
/********** FINE QUI VIENE DEFINITO QUALE DELLE COLONNE FAR VEDERE NELL'ELENCO NELLA PAGINA ADMIN E CON QUALE LABEL DI INTESTAZIONE ***********/

/********** QUI VIENE DEFINITO COSA STAMPARE NELLE COLONNE DELL'ELENCO NELLA PAGINA ADMIN ***********/
add_action('manage_posts_custom_column', 'print_animals_admin_columns');
function print_animals_admin_columns($column)
{
  switch($column) {
    case 'animal_name':
      echo esc_html(get_post_meta(get_the_ID(), 'animal_name', true));
    break;
    case 'animal_type':
      echo esc_html(get_post_meta(get_the_ID(), 'animal_type', true));
    break;
    case 'animal_months':
      echo esc_html(get_post_meta(get_the_ID(), 'animal_months', true));
    break;
  }
}
/********** FINE QUI VIENE DEFINITO COSA STAMPARE NELLE COLONNE DELL'ELENCO NELLA PAGINA ADMIN ***********/

/********** QUI VIENE DEFINITO QUALI DELLE COLONNE SONO ORDINABILI **********/
add_filter('manage_edit-animals_sortable_columns', 'set_animals_sortable_columns');
function set_animals_sortable_columns($columns)
{
  $columns['animal_name'] = 'animal_name';
  $columns['animal_type'] = 'animal_type';
  $columns['animal_months'] = 'animal_months';
  return $columns;
}
/********** FINE QUI VIENE DEFINITO QUALI DELLE COLONNE SONO ORDINABILI **********/

/********** QUI VIENE DEFINITO QUALI COLONNE SONO DA ORDINARE PER STRINGA E QUALI PER NUMERO **********/
add_filter('request', 'set_animals_orderby_columns'); 
function set_animals_orderby_columns($vars)
{
  if(!is_admin())
    return $vars;

  $orderby_string = array(
    'animal_name',
    'animal_type'
  );

  $orderby_numeric = array(
    'animal_months'
  );

  if(isset($vars['orderby'])) {
    if(in_array($vars['orderby'], $orderby_string)) {
      $vars['meta_key'] = $vars['orderby'];
      $vars['orderby'] = 'meta_value';
    } elseif(in_array($vars['orderby'], $orderby_numeric)) {
      $vars['meta_key'] = $vars['orderby'];
      $vars['orderby'] = 'meta_value_num';
    }
  }

  return $vars;
}
/********** FINE QUI VIENE DEFINITO QUALI COLONNE SONO DA ORDINARE PER STRINGA E QUALI PER NUMERO **********/

/********** QUI VIENE DEFINITO CHE TEMPLATES USARE PER MOSTRARE LE PAGINE RELATIVE AI NUOVI POSTS **********/
add_filter('template_include', 'include_animals_template', 1);
function include_animals_template($template_path) {
  if (get_post_type() != 'animals')
    return $template_path;

  if (is_single()) {
    if ( $theme_file = locate_template( array ( 'single-animals.php' ) ) )
      return $theme_file;

    return plugin_dir_path( __FILE__ ) . '/single-animals.php';
  } elseif (is_archive()) {
    if ( $theme_file = locate_template( array ( 'archive-animals.php' ) ) )
      return $theme_file;
    
    return plugin_dir_path( __FILE__ ) . '/archive-animals.php';
  }

  return $template_path;
}
/********** FINE QUI VIENE DEFINITO CHE TEMPLATES USARE PER MOSTRARE LE PAGINE RELATIVE AI NUOVI POSTS **********/

?>
